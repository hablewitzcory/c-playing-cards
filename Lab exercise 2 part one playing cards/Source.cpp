#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank
{
	Two = 2, 
	Three,
	Four, Five,
	Six, Seven,
	Eight, Nine,
	Ten, Jack,
	Queen, King,
	Ace
};

enum Suit
{
	Heart,
	Diamond,
	Club,
	Spade
};

struct Card
{
	Rank rank;
	Suit suit;
};

int Main()
{
	Card d2; Card d3; Card d4; Card d5; Card d6; Card d7; Card d8; Card d9; Card d10; Card dj; Card dq; Card dk; Card da;
	Card h2; Card h3; Card h4; Card h5; Card h6; Card h7; Card h8; Card h9; Card h10; Card hj; Card hq; Card hk; Card ha;
	Card c2; Card c3; Card c4; Card c5; Card c6; Card c7; Card c8; Card c9; Card c10; Card cj; Card cq; Card ck; Card ca;
	Card s2; Card s3; Card s4; Card s5; Card s6; Card s7; Card s8; Card s9; Card s10; Card sj; Card sq; Card sk; Card sa;

	d2.rank = Two; h2.rank = Two; c2.rank = Two; s2.rank = Two;
	d2.suit = Diamond; h2.suit = Heart; c2.suit = Club; s2.suit = Spade;

	d3.rank = Three; h3.rank = Three; c3.rank = Three; s3.rank = Three;
	d3.suit = Diamond; h3.suit = Heart; c3.suit = Club; s3.suit = Spade;

	d4.rank = Four; h4.rank = Four; c4.rank = Four; s4.rank = Four;
	d4.suit = Diamond; h4.suit = Heart; c4.suit = Club; s4.suit = Spade;

	d5.rank = Five; h5.rank = Five; c5.rank = Five; s5.rank = Five;
	d5.suit = Diamond; h5.suit = Heart; c5.suit = Club; s5.suit = Spade;

	d6.rank = Six; h6.rank = Six; c6.rank = Six; s6.rank = Six;
	d6.suit = Diamond; h6.suit = Heart; c6.suit = Club; s6.suit = Spade;

	d7.rank = Seven; h7.rank = Seven; c7.rank = Seven; s7.rank = Seven;
	d7.suit = Diamond; h7.suit = Heart; c7.suit = Club; s7.suit = Spade;

	d8.rank = Eight; h8.rank = Eight; c8.rank = Eight; s8.rank = Eight;
	d8.suit = Diamond; h8.suit = Heart; c8.suit = Club; s8.suit = Spade;

	d9.rank = Nine; h9.rank = Nine; c9.rank = Nine; s9.rank = Nine;
	d9.suit = Diamond; h9.suit = Heart; c9.suit = Club; s9.suit = Spade;

	d10.rank = Ten; h10.rank = Ten; c10.rank = Ten; s10.rank = Ten;
	d10.suit = Diamond; h10.suit = Heart; c10.suit = Club; s10.suit = Spade;

	dj.rank = Jack; hj.rank = Jack; cj.rank = Jack; sj.rank = Jack;
	dj.suit = Diamond; hj.suit = Heart; cj.suit = Club; sj.suit = Spade;

	dq.rank = Queen; hq.rank = Queen; cq.rank = Queen; sq.rank = Queen;
	dq.suit = Diamond; hq.suit = Heart; cq.suit = Club; sq.suit = Spade;

	dk.rank = King; hk.rank = King; ck.rank = King; sk.rank = King;
	dk.suit = Diamond; hk.suit = Heart; ck.suit = Club; sk.suit = Spade;

	da.rank = Ace; ha.rank = Ace; ca.rank = Ace; sa.rank = Ace;
	da.suit = Diamond; ha.suit = Heart; ca.suit = Club; sa.suit = Spade;

	return 0;
}